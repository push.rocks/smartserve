import {LitElement, html, css} from 'lit';
import { customElement, property } from 'lit/decorators.js';

import * as plugins from './smartserve_web.plugins.js';

declare global {
  interface HTMLElementTagNameMap {
    'smartserve-infoscreen': SmartserveInfoscreen;
  }
}

@customElement('smartserve-infoscreen')
export class SmartserveInfoscreen extends LitElement {
  public static attach (textArg: string) {
    const infoscreen: SmartserveInfoscreen = document.createElement('smartserve-infoscreen');
    infoscreen.text = textArg;
    document.body.append(infoscreen);
    return infoscreen;
  }

  //INSTANCE
  @property()
  private show = false;

  @property()
  private text = 'Hello';

  @property()
  private success = false;

  public static styles = [
    css`
      * {
        box-sizing: border-box;
      }

      :host {
        
      }

      .mainbox {
        z-index: 1000;
        position: fixed;
        transition: all 0.3s;
        display: block;
        bottom: -50px;
        right: 0px;
        left: 0px;
        height : 50px;
        background: #222;
        color: #CCC;
        padding: 15px;
        border-top: 1px solid #e4002b;
        text-align: center;
        font-family: 'Roboto', sans-serif;
        color: #fff;
        background: #111;
        box-shadow: 0px -30px 30px rgba(0,0,0,1);
        opacity: 0;
      }

      @media (prefers-color-scheme: light) {
        .mainbox {
          color: #333;
          background: #eeeeee;
          box-shadow: 0px 0px 5px rgba(0,0,0,0.3);
        }
      }

      .show {
        bottom: 0px;
        opacity: 1;
      }

      .success {
        background: green;
        border-top: 1px solid green;
      }
    `
  ];

  public setText(textArg: string) {
    this.text = textArg;
    this.show = true;
    this.success = false;
  }

  public setSuccess(textArg: string) {
    this.text = textArg;
    this.show = true;
    this.success = true;
  }

  public hide() {
    this.show = false;
    this.success = false;
    this.text = '';
  }


  public render () {
    return html`
      <div class="mainbox ${this.show ? 'show' : ''} ${this.success ? 'success': ''}">${this.text}</div>
    `
  }

  public async firstUpdated(_changedProperties: Map<string | number | symbol, unknown>): Promise<void> {
    super.firstUpdated(_changedProperties);
    plugins.smartdelay.delayFor(0);
    this.show = true;
  }
}
