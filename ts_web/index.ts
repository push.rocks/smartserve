import * as plugins from './smartserve_web.plugins.js';
import { logger } from './smartserve_web.logger.js';
logger.log('info', `SmartServe-Devtools initialized!`);

import { SmartserveInfoscreen } from './smartserve_web.infoscreen.js';

export class ReloadChecker {
  public lastReload: string;
  public infoscreen: SmartserveInfoscreen;
  public backendConnectionLost = false;
  constructor() { }

  public async reload () {
    // this looks a bit hacky, but apparently is the safest way to really reload stuff
    window.location.reload();
  }

  /**
   * starts the reload checker
   */
  public async performReloadCheck () {
    this.lastReload ? null : this.lastReload = globalThis.smartserve.lastReload;
    let response: Response;

    try {
      const controller = new AbortController();
      plugins.smartdelay.delayFor(1500).then(() => {
        controller.abort();
      })
      response = await fetch('/smartserve/reloadcheck', {
        method: 'POST',
        signal: controller.signal
      });
    } catch (err: any) {

    }

    if (response?.status !== 200) {
      this.backendConnectionLost = true;
      logger.log('warn', `got a status ${response?.status}. checking again in 2 seconds.`);
      this.infoscreen ? this.infoscreen.setText('backend connection lost...') : this.infoscreen = SmartserveInfoscreen.attach('backend connection lost...');
      await plugins.smartdelay.delayFor(2000);
      this.performReloadCheck();
      return;
    }
    if (this.backendConnectionLost) {
      this.backendConnectionLost = false;
      this.infoscreen.setSuccess('regained connection to backend...');
      await plugins.smartdelay.delayFor(2000);

      this.performReloadCheck();
      return;
    }
    const responseText = await response.text();
    let reloadJustified = false;
    this.lastReload !== responseText ? reloadJustified = true : null;
    if (reloadJustified) {
      const reloadText = `about to reload ${globalThis.globalSw ? '(purging the sw cache first...)': ''}`
      this.infoscreen
        ? this.infoscreen.setText(reloadText)
        : this.infoscreen = SmartserveInfoscreen.attach(reloadText);
        if (globalThis.globalSw) {
          await globalThis.globalSw.purgeCache();
        }
      this.infoscreen.setText(`cleaned caches`);
      await plugins.smartdelay.delayFor(1000);
      this.reload();
      return;
    } else {
      if (this.infoscreen) {
        this.infoscreen.hide()
      }
      await plugins.smartdelay.delayFor(1000);
      this.performReloadCheck();
      return;
    }
  }
}

const reloadCheckInstance = new ReloadChecker();
reloadCheckInstance.performReloadCheck();
