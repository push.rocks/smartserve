/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartserve',
  version: '2.0.33',
  description: 'easy serving of static files'
}
