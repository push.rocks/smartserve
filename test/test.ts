import { tap, expect } from '@pushrocks/tapbundle';
import * as smartpath from '@pushrocks/smartpath';

import { SmartServe } from '../ts/index.js';

let testSmartServe: SmartServe;
tap.test('should create a valid instance of SmartServe', async () => {
  testSmartServe = new SmartServe({
    injectReload: true,
    port: 3000,
    serveDir: smartpath.get.dirnameFromImportMetaUrl(import.meta.url),
    watch: true,
  });
  expect(testSmartServe).toBeInstanceOf(SmartServe);
});

tap.test('should start to serve files', async (tools) => {
  await testSmartServe.start();
  await tools.delayFor(5000);
  await testSmartServe.reload();
  await tools.delayFor(5000);
  await testSmartServe.reload();
});

tap.test('should stop to serve files ', async (tools) => {
  await tools.delayFor(5000);
  await testSmartServe.stop();
});

tap.start();
